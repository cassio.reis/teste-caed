#language: en

  Feature: Verificação da funcionalidade de postagem de editais

  Scenario: Caso teste 1 - Verificar na tela de Processos seletivos abertos
      Given Acesso a pagina oficial do Caed
      When  Clicar em Processos seletivos
      And   clicar no botão Fechar no modal gerado
      Then  Verifique se existe na tela a opção Processo Seletivo 010_2020

  Scenario: Caso teste 2 - Verificar na tela de processos seletivos publicados
      Given Acesso a pagina oficial do Caed
      When  Clicar em Processos seletivos
      And   clicar no botão Fechar no modal gerado
      And  clicar em processos seletivos publicados
      Then  Verifique se existe na tela a opção Processo Seletivo 010_2020

  Scenario: Caso teste 3 - Verificar na tela de processos seletivos encerrados
      Given Acesso a pagina oficial do Caed
      When  Clicar em Processos seletivos
      And   clicar no botão Fechar no modal gerado
      And  clicar em processos seletivos encerrados
      Then  Verifique se existe na tela a opção Processo Seletivo 010_2020

  Scenario: Caso teste 4 - Enviar o para o formulário a descrição do processo seletivo
      Given Dado que o caso teste 2 é verdade, lendo a descrição e escrevendo no form
      When  Clicar em enviar
      Then  Deve aparecer a mensagem de confirmaçao