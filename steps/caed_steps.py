from behave import *
from page.caed_page import caedPage
from nose.tools import assert_equal
import time
import tempfile

caed_page = caedPage()

ff = tempfile.TemporaryFile(mode='w+')

class Attributes:
    str_target = "Processo Seletivo 010/2020"
    str_desc_text = ""

    def set_desc(self, text):
        self.str_desc_text = text


@given(u'Acesso a pagina oficial do Caed')
def step_impl(context):
    time.sleep(1)
    caed_page.acess_page(url="http://fundacaocaed.org.br/#!/pagina-inicial")
    caed_page.browser_clear()


@when(u'Clicar em Processos seletivos')
def step_impl(context):
    time.sleep(1)
    caed_page.click_button_processos()


@when(u'clicar no botão Fechar no modal gerado')
def step_impl(context):
    time.sleep(1)
    caed_page.click_button_modal()


@then(u'Verifique se existe na tela a opção Processo Seletivo 010_2020')
def step_impl(context):
    time.sleep(1)
    teste = caed_page.get_text_edital_resultado(Attributes.str_target)
    print(assert_equal(teste, Attributes.str_target))


@when(u'clicar na barra lateral a esquerda em Resultados Publicados.')
def step_impl(context):
    time.sleep(1)
    caed_page.click_button_resultados_publicados()


@when(u'clicar em processos seletivos publicados')
def step_impl(context):
    time.sleep(1)
    caed_page.click_button_resultados_publicados()


@then(u'A página deve ser aberta')
def step_impl(context):
    time.sleep(1)
    teste = caed_page.get_text_edital_resultado(Attributes.str_target)
    print(assert_equal(teste, Attributes.str_target))


@step("clicar em processos seletivos encerrados")
def step_impl(context):
    time.sleep(1)
    caed_page.click_button_resultados_encerrados()


@given("Dado que o caso teste 2 é verdade, lendo a descrição e escrevendo no form")
def step_impl(context):
    time.sleep(1)
    caed_page.verify("http://fundacaocaed.org.br/#!/pagina-inicial", Attributes.str_target)

@when("Clicar em enviar")
def step_impl(context):
    caed_page.click_button_send_form()


@then("Deve aparecer a mensagem de confirmaçao")
def step_impl(context):
    text = caed_page.sucess()
    assert_equal(text, "Sua resposta foi registrada.")