from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
import time
import pdb

from browser import Browser


class caedPageLocator(object):
    #SELETORES CSS
    INPUT_FORM = '[name="entry.1418286286"]'
    TEXT_MESSAGE = 'div[class="freebirdFormviewerViewResponseConfirmationMessage"]'
    BUTTON_SUBMIT_FORM = 'span[class="appsMaterialWizButtonPaperbuttonLabel quantumWizButtonPaperbuttonLabel exportLabel"]'
    BUTTON_PROCESSOS = 'a[href="#!/processoseletivos"]'
    BUTTON_MODAL  = 'button[class="btn btn-confirm"]'
    TITLE_RESULTADO = '[data-attrid="title"]'
    VERIFICA_EDITAL = 'div[class="title ng-binding"]'
    VERIFICA_DESCRICAO = 'div[class="descricao ng-binding"]'
    BUTTON_RESULTADOS_PUBLICADOS = 'button[href="#v-pills-profile"]'
    BUTTON_RESULTADOS_ENCERRADOS = 'button[href="#v-pills-messages"]'


class caedPage(Browser):

    """
        Função responsável por retornar o elemento na tela
        Input: CSS Locator
        Return: Elemento
    """
    def get_element(self, locator):
        # CARREGA O ELEMENTO
        WebDriverWait(self.driver, 5).until(ec.visibility_of_element_located((By.CSS_SELECTOR, locator)))
        # RETORNA O ELEMENTO
        return self.driver.find_element(By.CSS_SELECTOR, locator)

    """
            Função responsável por retornar o elementos na telas
            Input: CSS Locator
            Return: Lista de elements
    """
    def get_elements(self, locator):
        WebDriverWait(self.driver, 5).until(ec.presence_of_all_elements_located((By.CSS_SELECTOR,locator)))
        return self.driver.find_elements(By.CSS_SELECTOR, locator)

    """
            Função que acessa uma pagina html
            Input: Url de acesso
    """
    def acess_page(self, url):
        # acessa url passada
        self.driver.get(url)

    """
            Função que retorna o texto de elemento web
            Input: Texto a ser procurado
            Output: texto encontrado, caso não encontre irá retornar vazio

    """
    def get_text_edital_resultado(self,target):
        time.sleep(10)
        elements = list(self.get_elements(caedPageLocator.VERIFICA_EDITAL))
        for element in elements:
            if(element.text == target ):
                return element.text

    """
            Função que um botão e realiza o click
    """
    def click_button_processos(self):
        button = self.get_element(caedPageLocator.BUTTON_PROCESSOS)
        button.click()

    """
                Função que um botão e realiza o click
    """
    def click_button_modal(self):
        #clica no botão fechar no modal
        button =self.get_element(caedPageLocator.BUTTON_MODAL)
        button.click()

    """
                Função que um botão e realiza o click
    """
    def click_button_resultados_publicados(self):
        button = self.get_element(caedPageLocator.BUTTON_RESULTADOS_PUBLICADOS)
        button.click()

    """
                Função que um botão e realiza o click
    """
    def click_button_resultados_encerrados(self):
        button = self.get_element(caedPageLocator.BUTTON_RESULTADOS_ENCERRADOS)
        button.click()

    """
                Função que retorna a descrição do elemento procurado
                input: texto alvo
                return texto do elemento
    """
    def get_text_descricao(self, str_target):
        time.sleep(10)
        elements = list(self.get_elements(caedPageLocator.VERIFICA_EDITAL))
        describes = list(self.get_elements(caedPageLocator.VERIFICA_DESCRICAO))
        cont =0
        for element in elements:
            if(element.text == str_target ):
                return describes[cont].text
            cont +=1

    """
                Função que faz o input do formulário
                input: texto alvo
    """
    def input_form(self,text):
        input = self.get_element(caedPageLocator.INPUT_FORM)
        input.send_keys(text)

    """
                Função que passa por todos os caminhos do caso teste 2
    """
    def verify(self, url, text):
        self.driver.get(url)
        self.browser_clear()
        self.click_button_processos()
        self.click_button_modal()
        self.click_button_resultados_publicados()
        a= str(self.get_text_descricao(text))
        self.driver.get("https://forms.gle/EXLo6WubFhACspcA9")
        self.input_form(a)

    """
                Função que um botão e realiza o click
    """
    def click_button_send_form (self):
        button = self.get_element(caedPageLocator.BUTTON_SUBMIT_FORM)
        button.click()

    """
                Função que retorna um texto na tela
                return: texto de sucesso
    """
    def sucess(self):
        element = self.get_element(caedPageLocator.TEXT_MESSAGE)
        return element.text