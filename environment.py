from browser import Browser
import time

BEHAVE_DEBUG_ON_ERROR = True



def before_all(context):
    context.browser = Browser()

def after_all(context):
    context.browser.browser_quit()

def after_scenario (context, scenario):
    time.sleep(3)
    context.browser.browser_clear()

