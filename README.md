# Teste de Software automatizado utilizando behave no Python

O repositório contêm os arquivos necessários para execução dos testes. Foi utilizado a biblioteca Behave e a biblioteca Selinum no Python.



## Como instalar?

Gentileza instalar Anaconda caso esteja utilizando Windows e o Python no Linux.
Acessar o projeto e realizar o comando pip install -r requeriments.txt

Acessar a pasta tests e executar o comando behave no terminal.




## Arquivos de documentação

Teste - Caed -> Apresentação do relatório de testes

Doc - Respostas -> Respostas do questionário 1




